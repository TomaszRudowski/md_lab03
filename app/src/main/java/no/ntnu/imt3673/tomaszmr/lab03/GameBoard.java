package no.ntnu.imt3673.tomaszmr.lab03;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class GameBoard extends View {

    private static final int TOP_SIDE = 1;
    private static final int LEFT_SIDE = 2;
    private static final int BOTTOM_SIDE = 3;
    private static final int RIGHT_SIDE = 4;

    private static final int MOVE_SPEED = 30;
    private static final int BOUNCE_SPEED = 15;

    private static final int BORDER_SIZE = 50;
    private static final int BOARD_COLOR = 0xff550055;

    private static final int BALL_COLOR = 0xff74AC23;
    private static final int BALL_RADIUS = 30;

    private final ShapeDrawable ball;
    private final ShapeDrawable board;

    private boolean ballInitialized;
    private boolean bouncing;

    private int ballX;
    private int ballY;

    private final ToneGenerator toneGenerator;


    public GameBoard(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.ball = new ShapeDrawable(new OvalShape());
        this.board = new ShapeDrawable(new RectShape());

        // Update view every 20ms
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                postInvalidate();
            }
        }, 0, 20);

        this.toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
    }

    /**
     * used to ignore sensor readings while bouncing
     * @return true if still generating ball bounce
     */
    public boolean isBouncing() {
        return bouncing;
    }

    /**
     * draw view
     * @param canvas host draw calls
     */
    protected void onDraw(Canvas canvas) {
        this.board.draw(canvas);
        this.ball.draw(canvas);
    }

    /**
     * Get focus first to find out screen size
     * @param hasWindowFocus true if current Window of the activity gains focus
     */
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        initBall();
        initBoard();
    }

    /**
     * init rectangle in the middle to be used as a board
     */
    private void initBoard() {
        this.board.setBounds(BORDER_SIZE,
                BORDER_SIZE,
                this.getWidth() - BORDER_SIZE ,
                this.getHeight() - BORDER_SIZE);
        this.board.getPaint().setColor(BOARD_COLOR);
    }

    /**
     * init a ball in the middle, set color and size according to const
     */
    private void initBall() {
        this.ballX = this.getWidth() / 2;
        this.ballY = this.getHeight() / 2;

        this.ball.getPaint().setColor(BALL_COLOR);
        this.ball.setBounds(
                this.ballX - BALL_RADIUS,
                this.ballY - BALL_RADIUS,
                this.ballX + BALL_RADIUS,
                this.ballY + BALL_RADIUS
        );
        this.ballInitialized = true;
    }

    /**
     * move a ball based on results from sensor.
     * Checks if new coordinates would move the ball outside board.
     *
     * @param orientationAngles represents the device's orientation
     * @return true if moved without bouncing
     */
    public boolean moveBall( float[] orientationAngles) {
        if (!this.ballInitialized) {
            return false;
        }
        int newX = this.ballX - (int) (orientationAngles[1] * MOVE_SPEED);
        int newY = this.ballY - (int) (orientationAngles[2] * MOVE_SPEED);

        int centerLimit = BORDER_SIZE + BALL_RADIUS;
        if (newX < centerLimit) {
            System.out.println("right");
            reachedSide(orientationAngles, RIGHT_SIDE);
            return false;
        }
        if (newX > this.getWidth() - centerLimit) {
            System.out.println("left");
            reachedSide(orientationAngles, LEFT_SIDE);
            return false;
        }

        if (newY < centerLimit) {
            System.out.println("bottom");
            reachedSide(orientationAngles, BOTTOM_SIDE);
            return false;
        }
        if (newY > this.getHeight() - centerLimit) {
            System.out.println("top");
            reachedSide(orientationAngles, TOP_SIDE);
            return false;
        }

        this.ballX = newX;
        this.ballY = newY;

        this.ball.setBounds(
                this.ballX - BALL_RADIUS,
                this.ballY - BALL_RADIUS,
                this.ballX + BALL_RADIUS,
                this.ballY + BALL_RADIUS
        );
        return true;
    }

    /**
     * to create correct bounce angle and speed(force) of the ball
     * need to reverse results from sensors and call bouncing method
     * Sets this.bouncing flag to lock/unlock sensor listener
     * @param orientationAngles represents the device's orientation
     * @param side const that represents device side
     */
    private void reachedSide (float[] orientationAngles, int side) {
        this.bouncing = true;
        switch (side) {
            case TOP_SIDE:
                // flip Y
                orientationAngles[2] *= -1.0;
                break;
            case LEFT_SIDE:
                // flip X
                orientationAngles[1] *= -1.0;
                break;
            case BOTTOM_SIDE:
                // flip Y
                orientationAngles[2] *= -1.0;
                break;
            case RIGHT_SIDE:
                // flip X
                orientationAngles[1] *= -1.0;
                break;
        }
        bounceBall(orientationAngles);
        this.bouncing = false;
    }

    /**
     * pretend that the parameter is a result from the sensor and try to adjust the ball
     * coordinates BOUNCE_SPEED number of times.
     * Last recorded data from sensor make it differentiate speed/range of bouncing
     * When hitting the edge of the board close enough to a corner need to stop current method call
     * and rely on that the last moveBall call creates another reachedSide call.
     * To consider hitting presisely corner coordinates could cause problems.
     * @param orientationAngles represents the device's orientation
     */
    private void bounceBall (float[] orientationAngles) {
        generateNotification();
        for (int i = 0 ; i < BOUNCE_SPEED ; i++) {
            if ( ! moveBall(orientationAngles) ) {
                // reached side again, must skip the rest
                System.out.println("Reached side again...stopping the first call");
                return;
            }
        }
    }

    /**
     * notification called when bounceBall method called (i.e. ball hits the edge of game board)
     */
    private void generateNotification() {
        System.out.println("Bouncing ");
        vibrate();
        ping();
    }

    /**
     * https://stackoverflow.com/questions/13950338/how-to-make-an-android-device-vibrate
     */
    private void vibrate(){
        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 100 milliseconds
        assert v != null;
        v.vibrate(100);
    }

    /**
     * https://developer.android.com/reference/android/media/ToneGenerator.html
     */
    private void ping(){
        this.toneGenerator.startTone(ToneGenerator.TONE_SUP_PIP, 100);
    }

}
