package no.ntnu.imt3673.tomaszmr.lab03;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor sensor;
    private GameBoard gameBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.gameBoard = findViewById(R.id.v_board);
        setupSensors();

    }

    /**
     * unregisters listener to stop reacting on sensor data
     */
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this, this.sensor);
    }

    /**
     * registers listener to resume reacting on sensor data
     */
    @Override
    protected void onResume() {
        super.onResume();
        this.sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * initialize sensor manager and rotation sensor
     */
    private void setupSensors() {
        this.sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        assert sensorManager != null;
        this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        this.sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     *  handles events from sensor.
     *  First converts a rotation vector to a rotation matrix
     *  Later uses it to compute the device's orientation based on the rotation matrix.
     * @param sensorEvent here only rotation vector sensor type events.
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            final float[] rotationMatrix = new float[9];
            SensorManager.getRotationMatrixFromVector(rotationMatrix, sensorEvent.values);
            final float[] orientationAngles = new float[3];
            SensorManager.getOrientation(rotationMatrix, orientationAngles);

            // ignore sensor while bouncing
            if (!this.gameBoard.isBouncing()) {
                this.gameBoard.moveBall(orientationAngles);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

}
