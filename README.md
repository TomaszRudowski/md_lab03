
### lab 4 ###

### Settings ###

With adjusting constatnts MOVE_SPEED and BOUNCE_SPEED it is possible to steer ball velocity when flipping a phone, and height for bouncing(while bouncing sensor readings are ignored)

### Lint warnings ###

* Lint: Correctness - (2 warn) - Kept older versions of Gradle dependencies, target API (26)

* Lint: Usability - (1 warn) - "App not indexable by Google Search" - skipping implementing support for Firebase App Indexing

* spelling - skipping